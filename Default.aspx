﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="cargaApi.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
<title>:: Swift - Real Estate ::</title>
<!-- Favicon-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css"/>
<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<!-- Custom Css -->
<link href="assets\css\main.css" rel="stylesheet"/>
<link href="assets\css\login.css" rel="stylesheet"/>

<!-- Swift Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="assets\css\themes\all-themes.css" rel="stylesheet"/>
</head>
<body class="login-page authentication">

<div class="container">
    <div class="card-top"></div>
    <div class="card">
        
        <div class="col-md-12">
            <form id="sign_in" method="POST">
       
                <div>
               
                    <div class="text-center">
                        <a href="Consulta.aspx" class="btn btn-raised waves-effect g-bg-blue" type="submit">Consultar Processo</a>
                        <a href="graficos.aspx" class="btn btn-raised waves-effect" type="submit">Graficos</a>
                    </div>
                  
                </div>
            </form>
        </div>
    </div>    
</div>
<div class="theme-bg"></div>

<!-- Jquery Core Js --> 
<script src="assets\bundles\libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets\bundles\vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<script src="assets\bundles\mainscripts.bundle.js"></script><!-- Custom Js --> 
<script src="assets\js\pages\examples\sign-in.js"></script>
</body>
</html>
