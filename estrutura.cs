﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
namespace cargaApi
{
    public class estrutura
    {
        public ObjectId _id { get; set; }
        public string numeroDoProcesso { get; set; }
        public DateTime  dataDeCriacaoDoProcesso { get; set; }
        public decimal valor { get; set; }
        public string escritorio { get; set; }
    }
}