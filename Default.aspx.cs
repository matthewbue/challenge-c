﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using MongoDB.Driver;
using System.Configuration;

namespace cargaApi
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string json = File.ReadAllText(Server.MapPath("./") + "processo.json");
            JsonTextReader reader = new JsonTextReader(new StringReader(json));

            MongoClient client = new MongoClient(
               ConfigurationManager.AppSettings["conexaocarga"]);
            IMongoDatabase db = client.GetDatabase("DBCatalogo");

            Console.WriteLine("Icluindo Processos...");
            var cargaprocesso = db.GetCollection<estrutura>("processos");
            //Teste Carga Mongo DB

            //proc01.numeroprocesso = "proc001";
            //proc01.escritorio = "sebastiao";
            //proc01.valor = 35;
            //proc01.dataDeCriacaoDoProcesso = DateTime.Today;



            estrutura p = new estrutura();
            while (reader.Read())
            {
               
                if (reader.Value != null)
                {
                    if (reader.Value.ToString().Trim() == "numeroDoProcesso")
                    {
                        reader.Read();
                      
               
                        p.numeroDoProcesso += reader.Value;
                        

                    }
                    else if (reader.Value.ToString().Trim() == "valor")
                    {
                        reader.Read();
             
                  
                        p.valor = Convert.ToDecimal( reader.Value);
             
                    }
                    else if (reader.Value.ToString().Trim() == "escritorio")
                    {
                        reader.Read();
                        
                   
                              p.escritorio += reader.Value;
                    
                    }
                    else if (reader.Value.ToString().Trim() == "dataDeCriacaoDoProcesso")
                    {
                        reader.Read();
                        
                     
                        p.dataDeCriacaoDoProcesso = Convert.ToDateTime(reader.Value);
                     
                    }
                }
            }
            cargaprocesso.InsertOne(p);
        
        }
    }
}

            
        
    